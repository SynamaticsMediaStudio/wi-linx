@echo off

REM SETTINGS
REM Set the title of the command prompt window
title Linux Spark
REM Set the color of the command prompt window
COLOR FA
REM SYSTEM INFO
REM Set the version number of the script
set "version=1.0"
REM Set the email address of the script author
set "email=synamatics@gmail.com"

REM UNIT TESTS
REM Run unit tests to verify the script's functionality
echo Running unit tests...
echo.

REM TEST 1: Test that the script can handle a missing putty key file
REM Test that the script can detect when a putty key file is missing
echo Test 1: Testing that the script can handle a missing putty key file...
echo.
REM Create a test file with a missing putty key file
(
    echo "missing.ppk" "example.com"
) > test1.txt
REM Call the run_test subroutine to run the test
call :run_test test1.txt "Putty key file not found: missing.ppk" "Server reboot success: example.com"
echo.

REM TEST 2: Test that the script can handle a server reboot failure
REM Test that the script can detect when a server fails to reboot
echo Test 2: Testing that the script can handle a server reboot failure...
echo.
REM Create a test file with a server that fails to reboot
(
    echo "example.ppk" "failure.com"
) > test2.txt
REM Call the run_test subroutine to run the test
call :run_test test2.txt "Server reboot failed: failure.com" "Server reboot success: failure.com"
echo.

REM TEST 3: Test that the script can handle a server reboot success
REM Test that the script can detect when a server reboots successfully
echo Test 3: Testing that the script can handle a server reboot success...
echo.
REM Create a test file with a server that reboots successfully
(
    echo "example.ppk" "success.com"
) > test3.txt
REM Call the run_test subroutine to run the test
call :run_test test3.txt "Server reboot success: success.com" "Server reboot success: success.com"
echo.

REM END OF UNIT TESTS

goto END


:run_test
REM This subroutine runs a unit test and checks the output against expected output
set "test_file=%~1"
set "expected_error=%~2"
set "expected_output=%~3"
set "error_output="
set "output_output="

REM Run the test by reading the test file line by line and executing the commands
(
    for /f "usebackq tokens=1,2 delims= " %%a in ("%test_file%") do (
        set "server=%%b"
        set "key_file=%%a"
        if not exist "%key_file%" (
            REM If the putty key file is missing, set the error output to indicate this
            echo Putty key file not found: %key_file%
            set "error_output=Putty key file not found: %key_file%"
            goto :error_check
        )
        REM Run the command to simulate a server reboot and check the exit code
        plink -t "%server%" -i "%key_file%" "exit 1" > nul 2>&1
        if %errorlevel% equ 0 (
            REM If the server reboots successfully, set the output output to indicate this
            echo Server reboot success: %server%
            set "output_output=Server reboot success: %server%"
        ) else (
            REM If the server fails to reboot, set the output output to indicate this
            echo Server reboot failed: %server%
            set "output_output=Server reboot failed: %server%"
        )
    )
) > nul

:error_check
REM Check the error and output outputs against the expected outputs
if not "%error_output%"=="%expected_error%" (
    REM If the error output does not match the expected output, indicate that the test failed
    echo Test failed: Error output does not match expected output
    echo Expected: %expected_error%
    echo Actual: %error_output%
    exit /b 1
)
if not "%output_output%"=="%expected_output%" (
    REM If the output output does not match the expected output, indicate that the test failed
    echo Test failed: Output output does not match expected output
    echo Expected: %expected_output%
    echo Actual: %output_output%
    exit /b 1
)
REM If the outputs match the expected outputs, indicate that the test passed
echo Test passed
exit /b 0

:END

