# Linux Spark: Reboot Script

This project contains a bash script and a Windows batch file for rebooting Linux servers via SSH.

## Installation

### Linux

1. Install PuTTY and plink on your Linux machine.
2. Place the servers to be rebooted in a file named `servers.txt`. Each line should contain the path to the PuTTY key file, followed by the server address and port number, separated by spaces. For example:

   

