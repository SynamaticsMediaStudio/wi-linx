#!/bin/bash

# SETTINGS
# Title of this script for display purposes
declare -r TITLE="Linux Spark"
# Version of this script for display purposes
declare -r VERSION="1.0"
# Email address of the person who wrote this script
declare -r EMAIL="synamatics@gmail.com"

# Test reboot of servers
# This function will test the reboot.sh script
# It will set up a test server with a known Putty key file and IP address
# Then it will call reboot.sh with a test servers.txt file that contains the test server
# Finally, it will check the output of reboot.sh and print a message indicating whether the test succeeded or failed
test_reboot_server() {
    # Set up test server
    # Create a test server with a known Putty key file and IP address
    # Use Docker to create a lightweight CentOS container
    # Copy the Putty key file into the container
    # Start the container and expose port 22
    rm -rf test-reboot-server
    docker build -t test-reboot-server --build-arg SSH_KEY_FILE=$1 .
    docker run -p 2222:22 -v $1:/root/.ssh/test.ppk test-reboot-server
    # Run reboot.sh with test servers.txt file
    # Use the test Putty key file and IP address
    # Pipe the output to grep to check for success message
    # Get the exit code of the command
    # If the exit code is 0, print a success message
    # If the exit code is non-zero, print a failure message
    # Clean up the test server
    REBOOT_OUTPUT=$(./reboot.sh <<< $2 | grep "Server reboot success" | wc -l)
    if [ "$REBOOT_OUTPUT" -eq 1 ]; then
        echo "SUCCESS: test_reboot_server $1 $2"
    else
        echo "FAILED: test_reboot_server $1 $2"
    fi
    rm -rf test-reboot-server
}

# Test reboot.sh
# Run test_reboot_server with known inputs
# The first argument is the path to the Putty key file
# The second argument is the contents of the servers.txt file
test_reboot_server "./test.ppk" "centos@test-reboot-server -i ./test.ppk -P 2222"

